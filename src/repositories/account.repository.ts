import {DefaultCrudRepository} from '@loopback/repository';
import {Account} from '../models';
import {MongoDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class AccountRepository extends DefaultCrudRepository<
  Account,
  typeof Account.prototype.id
> {
  constructor(
    @inject('datasources.mongo') dataSource: MongoDataSource,
  ) {
    super(Account, dataSource);
  }
}
