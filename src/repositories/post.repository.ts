import {DefaultCrudRepository} from '@loopback/repository';
import {Post} from '../models';
import {MongoDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class PostRepository extends DefaultCrudRepository<
  Post,
  typeof Post.prototype.id
> {
  constructor(
    @inject('datasources.mongo') dataSource: MongoDataSource,
  ) {
    super(Post, dataSource);
  }
}
